import React from 'react';
import { useForm } from 'react-hook-form';


function BasicFormComponent() {

    let { register, handleSubmit, watch, errors } = useForm();

    let onSubmit = (data) => {
        console.log(data);
    }

    console.log(watch("password"));

    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)} >
                <input name="email" type="email" ref={ register({ required: true }) } required /><br />

                <input name="password" type="password" ref={register} /><br />
                { errors.email && <span>This field is required</span>}<br />

                <input type="submit" />
            </form>
        </>
    )
}

export default BasicFormComponent;