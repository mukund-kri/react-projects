import React from 'react';

import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

import BasicFormComponent from './BasicFormComponent';

import './App.css';

function App() {
  return (
    <>
      <h3>Welcome to react-hook-form examples</h3>
      <ul>
        <li><a href="/basic_example">Basic Example</a></li>
      </ul>

      <Router>
        <Switch>
          <Route path="/basic_example" component={BasicFormComponent} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
