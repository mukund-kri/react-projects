(ns ^:figwheel-no-load state.dev
  (:require
    [state.core :as core]
    [devtools.core :as devtools]))


(enable-console-print!)

(devtools/install!)

(core/init!)
