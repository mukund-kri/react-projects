(ns state.core
    (:require
      [reagent.core :as r]))

;;;; Component state
;; Example of using state at the component level. Example repeated from the
;; previous (components) example.
(defn click-counter []
  (let [clicks (r/atom 0)]
    (fn []
      [:div
       [:h2 "Component state example"]
       [:span @clicks]
       [:button {:on-click #(swap! clicks inc)} "Click Me!"]])))

;;;; Global state example
(defonce state (r/atom ""))

(defn show-text []
  [:div @state])

(defn update-txt []
  [:input
   {:type "text"
    :value @state
    :on-change #(reset! state (-> % .-target .-value))}])

(defn show-and-update []
  [:div
   [:h2 "Global state example"]
   [show-text]
   [update-txt]])

;;;; Parent and Child components - Sharing state.
;; A slight modification of the example above.
(defn show-text-2 [text]
  [:div @text])

(defn update-text-2 [text]
  [:input
   {:type "text"
    :value @text
    :on-change #(reset! text (-> % .-target .-value))}])

(defn parent-child-state []
  (let [text (r/atom "initial text")]
    (fn []
      [:div
       [show-text-2 text]
       [update-text-2 text]]
      )))

;; -------------------------
;; Initialize app
(defn mount-root []
  (r/render
   [:div
    [click-counter]
    [show-and-update]
    [parent-child-state]]
   (.getElementById js/document "app")))

(defn init! []
  (mount-root))
