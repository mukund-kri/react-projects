(ns ^:figwheel-no-load components.dev
  (:require
    [components.core :as core]
    [devtools.core :as devtools]))


(enable-console-print!)

(devtools/install!)

(core/init!)
