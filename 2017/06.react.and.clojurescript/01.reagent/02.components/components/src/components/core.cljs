(ns components.core
    (:require
      [reagent.core :as r]))

;; -------------------------
;; Views

;; Form-1 views a plain function 
(defn home-page []
  [:div
   [:h2 "Form-1 component"]
   [:p "This type of views is just a plain function"]
   ])

;; Form-2 views. A function that returns a function
;; Typical uses cases are the inner render function closes over the state atom.
(defn click-counter []
  (let [clicks (r/atom 0)]
    (fn []
      [:div
       [:h2 "Form-2 component"]
       [:span @clicks]
       [:button {:on-click #(swap! clicks inc)} "Click Me!"]])))


(defn toggle [state]
  (if (= state :started)
    :stopped
    :started))

;; Form-3 components. Classes with lifecycle methods.
(defn toggle-state []
  (let [state (r/atom :started)]
    (r/create-class
     {:component-did-mount
      #(println "In component did mount")

      :component-will-mount
      #(println "In Component will mount")

      :component-will-update
      #(println "In component will update")

      :reagent-render
      (fn []
        [:div
         [:h2 "Form-3 component"]
         [:p (if (= @state :started)
               "Started State"
               "Stopped State")]
         [:button {:on-click #(swap! state toggle)} "Toggle!"]])
      
      })))

;; -------------------------
;; Initialize app

(defn mount-root []
  (r/render
   [:div
    [home-page]
    [click-counter]
    [toggle-state]]
   (.getElementById js/document "app")))

(defn init! []
  (mount-root))

