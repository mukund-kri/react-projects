(ns ^:figwheel-no-load basics.dev
  (:require
    [basics.core :as core]
    [devtools.core :as devtools]))


(enable-console-print!)

(devtools/install!)

(core/init!)
