# Reagent Basics

Basic reagent code. The very basic examples in reagent. All the component are
form-1 components (plain functions). The following topics are demonstrated ...

1. Form-1 components
2. Props / Paramenters
3. Simple todo (display only) example 


### Development mode

To start the Figwheel compiler, navigate to the project folder and run the 
following command in the terminal:

```
lein figwheel
```

Figwheel will automatically push cljs changes to the browser.
Once Figwheel starts up, you should be able to open the `public/index.html` 
page in the browser.


### Building for production

```
lein clean
lein package
```
