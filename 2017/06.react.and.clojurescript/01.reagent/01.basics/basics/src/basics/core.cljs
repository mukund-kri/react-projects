(ns basics.core
    (:require
      [reagent.core :as r]))


;; -------------------------
;; Views

(defn my-component-basic []
  "A very basic component. Returns a p tag with text"
  [:p "Very basic component"])

;; parameterized (props in react) views
(defn titleize [text]
  "A view with parameters"
  [:h3 text])

;;;; A simple use-case. Display all the tasks in a list.
(def project-tasks
  ["Create project.clj",
   "Write src/project/core.cljs",
   "Run leiningen"])

(defn task-item-view [task]
  "Display individual task"
  [:li task])

(defn task-list-view [tasks]
  "Component that lists the tasks"
  [:div
   [:h2 "Task List"]
   [:ul
    (for [task tasks]
      [task-item-view task])]])

;; Main / Entry Point component
(defn home-page []
  [:div
   [:h2 "Welcome to Reagent -"]
   [my-component-basic]
   [task-list-view project-tasks]
   [titleize "parameterized component"]])


;; -------------------------
;; Initialize app

(defn mount-root []
  (r/render [home-page] (.getElementById js/document "app")))

(defn init! []
  (mount-root))
