(ns getting-stated.core
  (:require [reagent.core :as reagent]
            [re-frame.core :as re-frame]
            [getting-stated.events :as events]
            [getting-stated.views :as views]
            [getting-stated.config :as config]))


(defn dev-setup []
  (when config/debug?
    (enable-console-print!)
    (println "dev mode")))

(defn mount-root []
  (re-frame/clear-subscription-cache!)
  (reagent/render [views/main-panel]
                  (.getElementById js/document "app")))

(defn ^:export init []
  (re-frame/dispatch-sync [::events/initialize-db])
  (dev-setup)
  (mount-root))
