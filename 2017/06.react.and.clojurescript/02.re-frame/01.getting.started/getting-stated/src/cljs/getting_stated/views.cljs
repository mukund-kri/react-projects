(ns getting-stated.views
  (:require [re-frame.core :as re-frame]
            [getting-stated.subs :as subs]
            ))

(defn main-panel []
  (let [name (re-frame/subscribe [::subs/name])]
    [:div "Hello from " @name]))

