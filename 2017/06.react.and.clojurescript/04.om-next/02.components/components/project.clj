(defproject quick-start "0.0.1-SNAPSHOT"
  :description "TODO: add your description here"
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/clojurescript "1.10.238"]
                 [org.omcljs/om "1.0.0-beta3"]]

  :plugins [[lein-figwheel "0.5.15"]
            [lein-cljsbuild "1.1.7" :exclusions [[org.clojure/clojure]]]]

  :source-paths ["src"]

  :cljsbuild {:builds
              [{:id "dev"
                :figwheel true
                :source-paths ["src"]
                :compiler {:main "components.core"
                           :asset-path "js"
                           :output-to "resources/public/js/main.js"
                           :output-dir "resources/public/js"
                           :verbose true}}]}

  :figwheel {:css-dirs ["resources/public/css"]
             :nrepl-middleware ["cider.nrepl/cider-middleware"
                                "refactor-nrepl.middleware/wrap-refactor"
                                "cemerick.piggieback/wrap-cljs-repl"]}

  :profiles {:dev
             {:dependencies [[binaryage/devtools "0.9.9"]
                             [figwheel-sidecar "0.5.15"]
                             [com.cemerick/piggieback "0.2.2"]]

              :source-paths ["src" "dev"]

              :repl-options {:nrepl-middleware
                             [cemerick.piggieback/wrap-cljs-repl]}
              :clean-targets ^{:protect false} ["resources/public/js/compiled"
                                                :target-path]}}

  )

