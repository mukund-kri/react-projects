(ns quick-start.core
  (:require [goog.dom :as gdom]
            [om.next :as om :refer-macros [defui]]
            [om.dom :as dom]))

(enable-console-print!)
(println "This text ends up in the browser console")

;;;; A very simple component

;; use defui to create the component class
(defui HelloWorld
  Object
  (render [this]
          (dom/div nil "Hello World")))

;; make a factory out of the component class
(def hello (om/factory HelloWorld))

;; Hook the component on to the dom
(js/ReactDOM.render (hello) (gdom/getElement "app"))
