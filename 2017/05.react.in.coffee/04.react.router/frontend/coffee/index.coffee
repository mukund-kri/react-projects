import React    from 'react'
import ReactDOM from 'react-dom'

import App      from './components/App'
import css      from '../sass/main.sass'


ReactDOM.render(
  <App />,
  document.getElementById 'main'
)


