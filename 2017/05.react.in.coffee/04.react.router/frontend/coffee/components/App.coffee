import React, { Component } from 'react'
import PropTypes            from 'prop-types'
import {
  BrowserRouter as Router,
  Route,
  Link }                    from 'react-router-dom'

import Greeting             from './Greeting'
import Message              from './Message'


App = () ->
  <div>
    <Router>
      <div>
        <h1>Router Demo</h1>
        <p>Default Route: <Link to="/">Greeting</Link> </p>
        <p>Go To: <Link to="/message">Message Route</Link> </p>
          
        <Route exact path="/" component={Greeting} />
        <Route path="/message" component={Message} />
      </div>
    </Router>
  </div>

export default App

