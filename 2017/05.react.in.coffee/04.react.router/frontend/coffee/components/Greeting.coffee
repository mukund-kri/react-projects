import React, { Component } from 'react'
import PropTypes            from 'prop-types'


Greeting = ({name}) ->
  <h3>From Greeting Component :: Hello { name }</h3>

Greeting.propTypes =
  name: PropTypes.string.isRequired

Greeting.defaultProps =
  name: "John Doe"


export default Greeting
