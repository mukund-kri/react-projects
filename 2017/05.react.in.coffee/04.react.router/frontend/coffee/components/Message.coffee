import React, { Component } from 'react'
import PropTypes            from 'prop-types'

import Greeting             from './Greeting'


class Message extends Component

  constructor: (props) ->
    super props
    @state = name: props.name

  render: () ->
    <div>
      <h3>From Message Component :: Hello { @state.name }</h3>
      <Greeting name={ "Boss" }/>
    </div>

Component.propTypes =
  name: PropTypes.string.isRequired

Component.defaultProps =
  name: "John Doe"


export default Message
