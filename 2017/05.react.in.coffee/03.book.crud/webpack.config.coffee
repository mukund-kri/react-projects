###
Simple webpack config file. The features of this project is ...

1. Written primarlly in coffeescript 2.
2. React 16.
3. Webpack 3.6.
###

path = require 'path'


coffeeOpts =
  transpile:
    presets: ['env', 'react', 'stage-2']

config =
  # Entry point for the react app
  entry: './src/index.coffee'

  # Webpack output
  output:
    path: (path.resolve __dirname, 'build')
    filename: 'build.js'

  module:
    # use the coffee-loader when a coffeescript of coffee JSX file is encountered
    loaders: [
      test: /\.coffee$/
      loader: 'coffee-loader'
      exclude: /node-modules/
      options: coffeeOpts
    ,
      test: /\.coffeex$/
      loader: 'coffee-loader'
      exclude: /node-modules/
    ]
  resolve:
    extensions: ['.js', '.coffee', '.coffeex']

  devtool: 'source-map'

module.exports = config
  
