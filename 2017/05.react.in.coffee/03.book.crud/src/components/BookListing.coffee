# Display list of books

import React from 'react'

import BookItem from './BookItem'


export default BookListing = ({books, addBook, ...callbacks}) ->
  <div>
    <table>
      <tbody>
        {for book in books
          <BookItem book={ book } key={book.id} {callbacks...} /> }
      </tbody>
    </table>
    <button onClick={addBook}>Add New Book</button>
  </div>
