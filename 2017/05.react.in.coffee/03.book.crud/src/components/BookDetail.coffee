import React from 'react'


export default BookDetail = ({book}) ->
  <table>
    <tbody>
    <tr>
      <th>Title</th>
      <td>{ book.title }</td>
    </tr>
    <tr>
      <th>Author</th>
      <td>{ book.author }</td>
    </tr>
    </tbody>
  </table>
