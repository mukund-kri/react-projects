import React from 'react'


export default class BookEdit extends React.Component

  constructor: (props) ->
    super props
    @state = props.book

  onInputChange: (event) =>
    target = event.target
    name = target.name
    value = target.value

    @setState "#{name}": value

  save: (event) =>
    event.preventDefault()
    @props.updateBook @state.id, @state.title, @state.description, @state.author

  render: =>
    <form>
      <input
        name='title'
        type='text'
        placeholder='Book Title'
        value={ @state.title }
        onChange={ @onInputChange } />
      <br />
      <textarea
        name='description'
        placeholder='Book Description'
        value={ @state.description }
        onChange={ @onInputChange } />
      <br />
      <input
        name='author'
        type='text'
        placeholder='Author'
        value={ @state.author }
        onChange={ @onInputChange } />
      <br />
      <button onClick={ @save }>Save Book</button>
    </form>
