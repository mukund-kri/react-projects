# The 'control' component. This where the state is stored and the state
# mutations happen.

import React from 'react'

import BookListing from './BookListing'
import BookAdd     from './BookAdd'
import BookDetail  from './BookDetail'
import BookEdit    from './BookEdit'


export default class BookCRUD extends React.Component

  constructor: (props) ->
    super props

    @state =
      view: 'listing'           # by default show the listing view
      books: props.books        # initialize the state with props

  listingView: => @setState view: 'listing'
  
  showBook: (book) => @setState selectedBook: book, view: 'detail'

  addBook: => @setState view: 'add'

  delBook: (bookId) =>
    other = @state.books.filter (book) -> book.id != bookId
    @setState books: other

  editBook: (book) => @setState selectedBook: book, view: 'edit'

  saveBook: (title, description, author) =>
    [..., last] = @state.books
    newId = if last then last.id + 1 else 1
    book = {id: newId, title, description, author}
    @setState books: (@state.books.concat book), view: 'listing'

  updateBook: (id, title, description, author) =>
    book = (@state.books.filter (book) -> book.id == id)[0]

    if book
      book.title = title
      book.description = description
      book.author = author
    @setState view: 'listing'

  render: ->
    switch @state.view
      when 'detail' then <BookDetail book={ @state.selectedBook } />
      when 'edit' then <BookEdit book={ @state.selectedBook } updateBook={ @updateBook } />
      when 'add' then <BookAdd saveBook={ @saveBook }/>
      else <BookListing
        books={ @state.books }
        showBook={ @showBook }
        delBook={ @delBook }
        addBook={ @addBook }
        editBook={ @editBook }
        />
