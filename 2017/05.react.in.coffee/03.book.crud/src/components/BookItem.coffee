import React from 'react'


export default BookItem = ({book, showBook, delBook, editBook}) ->
  _showBook = -> showBook book
  _delBook  = -> delBook book.id
  _editBook = -> editBook book
  
  <tr>
    <td>{ book.title }</td>
    <td><button onClick={_showBook} >Show Details</button></td>
    <td><button onClick={_delBook} >Delete</button></td>
    <td><button onClick={_editBook} >Edit</button></td>
  </tr>
  
