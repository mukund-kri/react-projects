# Entry point into the react application. React is loaded into the webpage
# with this code

import React    from 'react'
import ReactDOM from 'react-dom'

import BookCRUD from './components/BookCRUD'

import books    from './data/books.json'


ReactDOM.render(
  <BookCRUD books={books} />,
  document.getElementById 'main'
)
