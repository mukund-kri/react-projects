# Project TIMESHEET APP

In this application you will be building a time sheet tracking application,
where a user can,

1. Use the timer to track the start and end time of a work session.
2. List her/his work sessions.
3. Set the hourly rate and generate an invoice of a work session.
