# 04.props

Shows the basics of passing in props to a component.

1. SimplePropsDemoStateless.jsx :: props and stateless react components.
1. SimplePropsDemoStateful.jsx :: props and statful react components.
1. BookComponent.jsx :: A full book object is sent to the component as a prop.
1. PersonComponent.jsx :: Slightly more advanced usage where, PropTypes are
set for type validation and defaultProps are set for default prop values.
