# Exercise on React component

## Visiting Card

Aim: Design and implement **Visiting Card** react component

General Steps you need to follow:

#### 1. DATA :: Design your input data.
  - object or independent props?
  - object design
  - props list
  - type checking
  - default values

#### CODE :: Code your component
  - use es6
  - separate file

#### STYLE
  - style your component with materialize (or any css framework) so that it looks
  like a Visiting Card

## Company ID Card

Aim: Design and implement **Company ID Card** react component

General Steps you need to follow:

#### 1. DATA :: Design your input data.
  - object or independent props?
  - object design
  - props list
  - type checking
  - default values

#### CODE :: Code your component
  - use es6
  - separate file

#### STYLE
  - style your component with materialize (or any css framework) so that it looks
  like a Company ID Card


## Multiple choice question

Aim: Design and implement **Multiple Choice Question** react component

A Multiple Choice Question can be one type of question in a Quiz App. Your
job is to design a component which displays this type of question.

**Note**: Don't worry about getting user input. That comes in a later chapter.

General Steps you need to follow:

#### 1. DATA :: Design your input data.
  - object or independent props?
  - object design
  - props list
  - type checking
  - default values

#### CODE :: Code your component
  - use es6
  - seperate file

#### STYLE
  - style your component with materialize (or any css framework) so that it looks
  like a Visiting Card


TODO: few more -- (website topbar, links sidebar with nav element)
