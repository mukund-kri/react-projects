/*
 * This component takes in a book object in the props and renders it.
 */
import React from 'react';


export default function({book}) {

  return (
    <div className="component">
      <h3>the book component</h3>
      <table className="component">
        <tbody>
          <tr>
            <th>Title</th>
            <td>{book.title}</td>
          </tr>
          <tr>
            <th>Description</th>
            <td>{book.description}</td>
          </tr>
          <tr>
            <th>Author</th>
            <td>{book.author}</td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}
