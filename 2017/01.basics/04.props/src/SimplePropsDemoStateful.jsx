/*
 * A simple stateful component, to show how porps are sent into a class based
 * stateful component.
 */

import React from 'react';

class SimplePropsDemoStateful extends React.Component {

  render() {
    return (
      <div className='component'>
        <h3>A Stateful Component</h3>
        <p>The values passed to prop1 :: <strong>{this.props.prop1}</strong></p>
        <p>The values passed to prop2 :: <strong>{this.props.prop2}</strong></p>
      </div>
    )
  }
}

export default SimplePropsDemoStateful;
