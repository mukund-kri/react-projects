/*
 * Demo the use of typechecking of props and default values.
 */
import React from 'react';
import PropTypes from 'prop-types';


class PersonComponent extends React.Component {
  render() {
    return (
      <div className='component'>
        <h3>Person Details Component</h3>
        <table className="component">
          <tbody>
            <tr>
              <th>First Name</th>
              <td>{this.props.first_name}</td>
            </tr>
            <tr>
              <th>Last Name</th>
              <td>{this.props.last_name}</td>
            </tr>
            <tr>
              <th>Gender</th>
              <td>{this.props.gender}</td>
            </tr>
            <tr>
              <th>Loged in as </th>
              <td>{this.props.username}</td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
};

/* --------------------------- TYPE CHECKING -------------------------------- */
PersonComponent.propTypes = {
  first_name: PropTypes.string.isRequired,
  last_name: PropTypes.string.isRequired,
  gender: PropTypes.oneOf(['female', 'male', 'other'])
};

/* --------------------------- PROP DEFAULTS -------------------------------- */
PersonComponent.defaultProps = {
  username: 'anonymous'
}

export default PersonComponent;
