/*
 * The entry point into my react app.
 */
import React from 'react';
import SimplePropsDemoStateless from './SimplePropsDemoStateless';
import SimplePropsDemoStateful from './SimplePropsDemoStateful';
import BookComponent from './BookComponent';
import PersonComponent from './PersonComponent';


const book = {
  title: 'React.js Essentials',
  description: `
A fast-paced guide to designing and building scalable and maintainable web
apps with React.jsAbout This Book- Build maintainable and performant user
interfaces for your web applications using React.js-`,
  author: 'Artemij Fedosejev '
}


class Main extends React.Component {

  render() {
    return (
      <div className='main-container'>
        <SimplePropsDemoStateless prop1="PROP 1" prop2="PROP2" />
        <SimplePropsDemoStateful prop1="SPROP 1" prop2="SPROP2" />

        { /* A more useful way to pass props. Pass in whole objects to be rendered */ }
        <BookComponent book={book} />

        { /* The following all throw warnings as the props are validated.
          You cans see these on the dev console of the browser. See The
          component code for the validations and default values.
          */}
          <PersonComponent
              first_name="John"
              gender="males"
              />

        {/* TODO: a few more calls which result in warnings */}

        {/* A correctly validated call. Also the username is set to default */}
        <PersonComponent
            first_name="John"
            last_name="Doe"
            gender="male"
            />
      </div>
    )
  }
}

export default Main;
