/*
 * Passing props into a stateless component. The props is just the first
 * argument to the function that's all.
 */
import React from 'react';


export default function({prop1, prop2}) {  // note how the props object is split
  return (
    <div className="component">
      <h3>A stateless component</h3>
      <p>The values passed in as prop1 is :: <strong>{prop1}</strong></p>
      <p>The values passed in as prop2 is :: <strong>{prop2}</strong></p>
    </div>
  )
}
