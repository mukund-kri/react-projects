/*
 * A stateful component that renders the time every second.
 * The current time is stored in the state and only this time is modified. The
 * rest of the work of montoring the state and re-rendering on change is left
 * to react.
 */

import React from 'react';


class Clock extends React.Component {

  // Overide the constructor to set up the initial state
  constructor(props) {
    super(props);  // So the React.Component has access to props

    // Initialize the state
    this.state = { time: new Date() };

    // Async update the date every second. This is the only place where the
    // state changes.
    setInterval(
      () => this.setState({ time: new Date() }),
      1000
    );
  }


  render() {
    return (
      <div className="component">
        <h1>Clock Component</h1>
        <h3>Time :: {this.state.time.toLocaleTimeString()}</h3>
      </div>
    )
  }
}

export default Clock;
