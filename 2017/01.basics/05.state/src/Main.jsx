/*
 * The entry point into my react app.
 *
 * These set of examples show the absolute basics of react state. All
 * stateful Component have a state object, which react monitors for chanage
 * and re-renders the component on change.
 */
import React from 'react';
import Clock from './Clock';


class Main extends React.Component {

  render() {
    return (
      <div className='main-container'>
        <Clock />
      </div>
    )
  }
}

export default Main;
