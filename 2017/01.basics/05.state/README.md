# The Component State

Simple usage of the component state. In the clock example the state is
modified and react figures out the changes and re-renders the component.
