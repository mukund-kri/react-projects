# 10 Parents And Children; State

This project goes into how the application state can be designed so that it
can be shared across the hierarchy of components.

#### CountDownTimer

This exactly the same as the ResetableCountdown component in project 06,
but in this case, its split into 3 component.

1. CountDownTimer :: The parent component and the one that holds the app state.
2. CountDownTimerDisplay :: Only displays the countdown and does nothing else.
3. CountDownTimerReset :: Only renders the reset button, and triggers the
event click. Note, the event is defined in the parent component and passed as
a prop to the child component.
