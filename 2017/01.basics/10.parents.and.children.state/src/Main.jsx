import React          from 'react';

// Import our local components
import CountDownTimer from './CountDownTimer';


// The main entry point to this application
class Main extends React.Component {

  render() {

    return (
      <div>
        <CountDownTimer startValue={ 200 } />
      </div>
    )
  }
}

export default Main;
