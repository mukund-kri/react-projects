/*
 * The same component as the one in ch 06. But now I'v split it into 3
 * seperate components.
 *
 * 1. CountDownTimer (this component), is the parent component which holds the
 * the other component and also the one where the state for the whole app is
 * maintained.
 *
 * 2. CountDownTimerDisplay, shows the countdown that all.
 *
 * 3. CountDownTimerReset, Holds the reset button and its event binding.
 *
 * Also note that in this source I have made an effort to not use semi-colons
 * as far as possible.
 */

import React from 'react'

import CountDownTimerDisplay from './CountDownTimerDisplay'
import CountDownTimerReset   from './CountDownTimerReset'

class CountDownTimer extends React.Component {

  constructor(props) {
    super(props)
    this.state = { value: props.startValue }
  }

  componentDidMount() {
    // start the countdown after the component is mounted.
    setInterval(
      () => this.setState({ value: this.state.value - 1 }),
      1000
    )
  }

  resetStartValue = (event) => {
    this.setState({ value: this.props.startValue })
  }

  render() {
    return (
      <div>
        <CountDownTimerDisplay value={ this.state.value } />
        <CountDownTimerReset resetStartValue={ this.resetStartValue } />
      </div>
    )
  }

}

export default CountDownTimer
