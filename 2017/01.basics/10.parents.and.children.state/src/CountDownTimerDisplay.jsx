import React from 'react'


export default class CountDownTimerDisplay extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return <h3>{ this.props.value }</h3>
  }
}
