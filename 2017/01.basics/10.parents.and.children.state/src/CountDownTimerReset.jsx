import React from 'react'

export default class CountDownTimerReset extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return <button onClick={ this.props.resetStartValue }> Reset Timer </button>
  }
}
