/*
 * A Component that demos how to use the following concepts to build a
 * Component.
 * 1. Events
 * 2. State
 * 3. Control Component
 */

import React from 'react';


class DisplayEditBookComponent extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      displayEditor: false,
      book: props.book
    };
  };

  showEditor = () => { this.setState({ displayEditor: true }) };
  showDisplay = () => { this.setState({ displayEditor: false }) };

  // Lifted directly from the react docs
  handleInputChange = (event) => {
     const target = event.target;
     const value = target.value;
     const name = target.name;

     let book = this.state.book;
     book[name] = value;

     this.setState({ book: book });
   };

  render() {
    var comp;
    if (this.state.displayEditor) {
      comp = <div>
      <table>
        <tbody>
          <tr>
            <th>Title</th>
            <td>
              <input
                name="title"
                type="text"
                value={ this.state.book.title }
                onChange={ this.handleInputChange }
              />
            </td>
          </tr>
          <tr>
            <th>Description</th>
            <td>
              <input
                name="description"
                type="text"
                value={ this.state.book.description }
                onChange={ this.handleInputChange }
              />
            </td>
          </tr>
          <tr>
            <th>Title</th>
            <td>
              <input
                name="author"
                type="text"
                value={ this.state.book.author }
                onChange={ this.handleInputChange }
                />
            </td>
          </tr>
        </tbody>
      </table>
        <button onClick={ this.showDisplay }>Save</button>
      </div>
    } else {
      comp = <div>
        <table>
          <tbody>
            <tr>
              <th>Title</th>
              <td>{ this.state.book.title }</td>
            </tr>
            <tr>
              <th>Description</th>
              <td>{ this.state.book.description }</td>
            </tr>
            <tr>
              <th>Title</th>
              <td>{ this.state.book.author }</td>
            </tr>
          </tbody>
        </table>
        <button onClick={ this.showEditor }>Edit</button>
      </div>
    }

    return (
      <div className="component">
        <h3>Display / Edit Book</h3>
        { comp }

      </div>
    )
  }
}


export default DisplayEditBookComponent;
