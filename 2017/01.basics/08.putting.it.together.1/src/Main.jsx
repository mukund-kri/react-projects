import React from 'react';

import DisplayEditBookComponent from './DisplayEditBookComponent';


const book = {
  title: "React.js Essentials",
  description: `A fast-paced guide to designing and building scalable and
  maintainable web apps with React.jsAbout This Book- Build maintainable and
  performant user interfaces for your web applications using React.js`,
  author: 'Artemij Fedosejev'
}


class Main extends React.Component {

  render() {
    return (
      <div className="main">
        <DisplayEditBookComponent book={ book } />
      </div>
    )
  }
}

export default Main;
