# 08 Putting It Together

In this section I put together all the concepts learned so far from the
previous projects.

#### DisplayEditBookComponent

This Component is able to display a book object and also edit it. It shows the
usage of the following concepts ...

1. Props and State
2. Events
3. Control Components

**NOTE ::** I have written this component as one big monolithic component,
which is not recommended. It should be broken down into a hierarchy of
components as we will see in the following sections.
