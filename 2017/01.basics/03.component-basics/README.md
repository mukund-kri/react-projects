# React Components, the very basics

Three basic examples of React components.

1. Without JSX, with plain JS API.
2. The newer `Stateless Component` which is nothing more than a function that
returns the JSX.
3. A full-blown, class based `Stateful Component`.
