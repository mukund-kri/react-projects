/*
 * The entry point into my react app.
 */
import React from 'react';
import NoJSXComponent from './NoJSXComponent';
import StatelessFunctionComponent from './StatelessFunctionComponent';
import StatefulComponent from './StatefulComponent';


class Main extends React.Component {

  render() {
    return (
      <div className='main-container'>
        <NoJSXComponent />
        <StatelessFunctionComponent name='Stateless Component'/>
        <StatefulComponent name='Stateful Component' />
      </div>
    )
  }
}

export default Main;
