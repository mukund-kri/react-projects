/*
 * Finally the common, stateful, class based, JSX component. This is the type
 * that's typically used.
 */
import React from 'react';


class StatefulComponent extends React.Component {
  render() {
    return (
      <div className='component'>
        A common Stateful Component with <em>Paramenter name = {this.props.name}</em>
      </div>
    )
  }
};

export default StatefulComponent;
