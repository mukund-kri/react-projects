/*
 * This component is Stateless ie. The state object does not exist in this
 * component.
 * It is uses the newer function syntax, where the component is defined as
 * a function rather than a class.
 */
 import React from 'react';
 

 const StatelessFunctionComponent = ({name}) => {

   return (
     <div className='component'>
      A Stateless function component with <em>Parameter name :: {name}</em>
     </div>
   )
 };

 export default StatelessFunctionComponent;
