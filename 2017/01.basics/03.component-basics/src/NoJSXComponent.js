/*
 * A pure js react component. No JSX is used here.
 * This is just for demo purposes. It is *ALLWAYS* recomended to use JSX wile
 * coding a component.
 *
 * It uses the React.createElement API directly to create a html element.
 * actully JSX is eventuall compiled down to this form.  
 */
import React from 'react';


class NoJSXComponent extends React.Component {
  render() {
    return React.createElement(
        'div',
        { className: 'component'},
        'This is a component coded in pure JS (no JSX)'
    )
  }
};

export default NoJSXComponent;
