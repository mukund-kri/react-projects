# 09 Parents and Children

This project deals with the absolute basics of splitting components into a
hierarchy of smaller component, each implementing a small portion of the
app.

#### BookListComponent

The UI functionality is implemented with two separate components.

1. The Book List component. Renders the entire list of books.
2. The Book Item component. Takes responsibility of rendering individual books in
the list.
