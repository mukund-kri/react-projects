/*
 * This component displays all the book in the list.
 * But in this case we break down the component into sub component. The
 * resposiblity of displaying an individual book is devolved to its own
 * component BookItemComponent. This component only deals with how
 * to display the list of books.
 */
import React from 'react';

// import the sub component
import BookItemComponent from './BookItemComponent';


export default ({ books }) => {
  const booksDisp = books.map(book => <BookItemComponent book={ book } key={ book.id } />)
  return (
    <div className='component'>
      <h3>Book Listing Component (composed of two components)</h3>
      <table>
        <thead>
          <tr>
            <th>
              Title
            </th>
            <th>
              Author
            </th>
          </tr>
        </thead>
        <tbody>
          { booksDisp }
        </tbody>
      </table>
    </div>
  )
}
