/*
 * This component takes resposiblity of displaying a single book item. That's
 * all.
 */
import React from 'react';


export default ({ book }) => {

  return (
    <tr>
      <td>{ book.title }</td>
      <td>{ book.author }</td>
    </tr>
  )
}
