import React             from 'react';

// Import our local components
import BookListComponent from './BookListComponent';

// Mocked data. Some sample books; in json format.
import books             from './books.json';


// The main entry point to this application
class Main extends React.Component {

  render() {

    return (
      <div>
        <BookListComponent books={ books } />
      </div>
    )
  }
}

export default Main;
