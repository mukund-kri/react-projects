/*
 * A simple webpack config file. Notice the extension of this file is
 * .babel.js instead of plain .js, the extra babel in the name let's us write
 * this config file in full es6.
 */

import path from 'path';

// This object tells webpack what to do and how to do it
const config = {

  // This file is where webpack starts it's work
  entry: path.resolve(__dirname, 'src/index.js'),

  // End result of webpack's work is dumped into this file
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'build.js'
  },

  module: {
    // Tells webpack which loader to use, based on the extension of the file.
    // In this case use babel for .js and .jsx files. Ignore every thing else.
    loaders: [
      { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
      { test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/ }
    ]
  },

  resolve: {
    // Allows use to ommit the extension when importing a file.
    // we just import / require .. './filename' instead of './filename.js' or
    // './filename.jsx'
    extensions: ['.jsx', '.js']
  },

  // Turn debugging on, so we can debug in es6 and not the transpiled es5
  devtool: "source-map"
}

export default config;
