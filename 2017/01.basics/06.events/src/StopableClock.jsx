/*
 * Same as the Clock example from project 05.state, but in this case the
 * clock can 'paused' on click of a button and can be restarted again on
 * clikc of the same button.
 */

import React from 'react';


class StopableClock extends React.Component {
   // Overide the constructor to setup the initial state. In this case the
   // state contains the current time and weather the clock is paused or not.
  constructor(props) {
    super(props);
    this.state = { paused: false,  time: new Date() };
  };

  // This a lifecycle method. We will cover the concept of lifecycle methods
  // the specific section on lifecycle methods.
  componentDidMount() {
    this.startClock();
  };

  // Start the clock
  startClock = () => {
    this.setState({ paused: false });
    this.timer = setInterval(
      () => this.setState({ time: new Date() }),
      1000
    );
  };

  // Pause the clock
  pauseClock = () => {
    this.setState({ paused: true });
    clearInterval(this.timer);
  };

  render() {
    let button = null;
    if (this.state.paused) {
      button = <button onClick={ this.startClock }>Restart Clock</button>
    } else {
      button = <button onClick={ this.pauseClock }>Pause Clock</button>
    }

    return (
      <div className="component">
        <h2>Stopable Clock Component</h2>
        <h3>Time :: {this.state.time.toLocaleTimeString()}</h3>
        { button }
      </div>
    )
  }
};

export default StopableClock;
