/*
 * A countdown timer that decrements and displays a number every second. It
 * can also has a reset button, which resets the countdown to the initial value.
 *
 * This is implemented as a stateful component.
 *
 * NOTE: The button and how it wired up to call the method 'reSet', onClick.
 */

import React from 'react';


class ResetableCountdown extends React.Component {

  // Overide the constructor to setup the initial state. In our case its just
  // the countdown value.
  constructor(props) {
    super(props);

    this.state = { value: props.initialNum };

    // also start the countdown
    setInterval(
      () => this.setState({ value: this.state.value - 1 }),
      1000
    );
  };

  // Called onClick of reset button
  reSet = (event) => {
    this.setState({ value: this.props.initialNum });
  };

  render() {
    return (
      <div className="component">
        <h2>Countdown Component</h2>
        <h3>{this.state.value}</h3>
        <button onClick={ this.reSet }>Reset Countdown</button>
      </div>
    )
  }
};

export default ResetableCountdown;
