import React from 'react';

import AlertComponent      from './AlertComponent';
import ConsoleLogComponent from './ConsoleLogComponent';
import EventsAndState      from './EventsAndState';
import ResetableCountdown  from './ResetableCountdown';
import StopableClock       from './StopableClock';


class Main extends React.Component {

  render() {
    return (
      <div>
        <AlertComponent />
        <ConsoleLogComponent  />
        <EventsAndState />
        <ResetableCountdown initialNum={ 200 } />
        <StopableClock />
      </div>
    )
  }
}

export default Main;
