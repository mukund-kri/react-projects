/*
 * Simple events example. Similar to the AlertComponent but in this case ...
 * - it is a class based stateful component
 * - writes a message to the console
 */

import React from 'react';


class ConsoleLogComponent extends React.Component {

  writeToConsole(e) {
    console.log("Event button clicked");
  }

  render() {
    return (
      <div className='component'>
        <button onClick={this.writeToConsole}>Write message to console</button>
      </div>
    )
  }
}


export default ConsoleLogComponent;
