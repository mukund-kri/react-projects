/*
 * Just count the number of times a user clicks the button. Serves as a simple
 * demo of events + state.
 */
import React from 'react';


class EventsAndState extends React.Component {

  constructor(props) {
    super(props);
    this.state = { counter: 0 };
  };

  incrementCounter = (event) => {
    this.setState({ counter: this.state.counter + 1 });
  }

  decrementCounter = (event) => {
    this.setState({ counter: this.state.counter - 1 });
  }

  render() {
    return (
      <div className="component">
        <h3>Click on buttons to increment / decrement count</h3>
        <p>Count :: <strong>{ this.state.counter }</strong></p>

        <button onClick = { this.incrementCounter }>Increment</button>
        <button onClick= { this.decrementCounter }>Decrement</button>
      </div>
    )
  }
}

export default EventsAndState;
