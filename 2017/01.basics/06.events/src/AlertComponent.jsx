/*
 * Simplest possible event and handler. Pop up a alert when the user
 * clicks a button.
 * This component is of the stateless function type.
 */
import React from 'react';


export default function() {

  function popUpAlert(e) {
    alert("You pressed the button");
  }

  return (
      <div className='component'>
        <button onClick={popUpAlert} >Click for alert</button>
      </div>
  )
}
