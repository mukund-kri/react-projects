# React + Webpack from scratch

## Explanation

The `EXPLANATION.md` in this folder dives into how I built this project.
Please read that first.

## Getting Started

This section gives you instruction on how to run this project.

* First install a the dependencies

```
> yarn install
```

* Next run the project with webpack-dev-server. Note: Peek into the
`package.json`, scripts section for the details

```
> yarn start
```

This will start a development web server and you can view the website at
http://localhost:8080/ 
