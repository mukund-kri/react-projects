# ReactJS + Webpack project structure

This document explains the important portions of this project structure, how
I built this project and what these files do.

## Software Dependencies

1. Nodejs
2. Yarn

## How I built this project

### Step 1 - Create a node package

I created a new node package with the follwoing command and accepting the
default values presented to me.

```
yarn init
```

### Step 2 - Install babel and presets

Add babel and the required presets to code this project in es6 and JSX.

```
yarn add babel babel-preset-env babel-preset-react --dev
```

### Step 3 - Install Webpack (v3.6) and plugins

```
yarn add webpack webpack-cli webpack-dev-server --dev
```

### Step 4 - Code .babelrc with presets

This is the full contents of my .babelrc, so that I can code in es6 with JSX.

```
{
  "presets": ["env", "react"]
}
```

### Step 5 - Code the webpack config

Next I wrote the webpack config file `webpack.config.babel.js`. The `.babel.`
is in the title is so that I can code this file in full es6. Go ahead and
read this file, I'v add a lot of comments in this file to make it more readable.

### Step 6 - Start developing

Next I added the `index.html` as the entry point, and `src` folder that contains
all the JS code. The project is now ready to go.

Read the README.md to run this project.
