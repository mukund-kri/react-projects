import React from 'react';
import ReactDOM from 'react-dom';

import Main from './Main.jsx';

ReactDOM.render(
  <Main name='Himanshu' />,
  document.getElementById('main')
)
