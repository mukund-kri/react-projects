import React from 'react';


class Main extends React.Component {

  render() {
    return (
      <h3>Hi! {this.props.name}</h3>
    )
  }
}

export default Main;
