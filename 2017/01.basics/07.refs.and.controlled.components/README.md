# 07.refs.and.controlled.components

This project has examples for the two methods in React, to capture user input,
like user input in forms.

#### ConfigurableTimerRefs

Show how to use `refs` to capture user input from the DOM.

#### ConfigurableTimerCC

Use `Controlled Components` to capture user input.
