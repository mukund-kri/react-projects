/*
 * This is direct copy of the example in the packt video series.
 * This is a countdown timer which is initialized with the number of seconds
 * to count down via the porps. There is a start button which starts / restarts
 * the countdown. And there is a input form where the user can specify the
 * the initial countdown number.
 *
 * The value of the input box is captured by react via a control component.
 * The control component is the recomended way to capture user input in React.
 */
import React from 'react';


class ConfigurableTimerCC extends React.Component {

  constructor(props) {
    super(props);
    this.state = { startCount: Number(props.count) };
  };

  startTimer = (event) => {
    this.setState({ count: this.state.startCount });
    this.timerID = setInterval(
      () => this.setState({ count: this.state.count - 1}),
      1000
    );
  };

  stopTimer = (event) => {
    if(this.timerID) {
      clearInterval(this.timerID)
    }
  };

  restartTimer = (event) => {
    this.stopTimer();
    this.startTimer();
  };

  handleInputChange = (event) => {
    this.setState({
      startCount: Number(event.target.value)
    })
  };

  render() {
    return (
      <div className="component">
        <h3>Countdown timer [Control Component Version]</h3>
        <h3>{ this.state.count }</h3>
        <div>
          <button onClick={ this.restartTimer } > (Re)Start Timer</button>
          <button onClick={ this.stopTimer }> Stop Timer </button>
        </div>
        <div>
          <input
            value={ this.state.startCount }
            onChange={ this.handleInputChange }
          />
          <button onClick={ this.restartTimer }>Restart with new value</button>
        </div>
      </div>
    )
  };
};

export default ConfigurableTimerCC;
