import React                 from 'react';
import ConfigurableTimerRefs from './ConfigurableTimerRefs';
import ConfigurableTimerCC   from './ConfigurableTimerCC';


class Main extends React.Component {

  render() {
    return (
      <div>
        <ConfigurableTimerRefs count="10" />
        <ConfigurableTimerCC count="10"/>
      </div>
    )
  }
}

export default Main;
