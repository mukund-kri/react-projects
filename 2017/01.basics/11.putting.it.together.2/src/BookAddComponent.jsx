/*
 * Add a new book to the book list.
 * Uses the recomended control component to implemnt the form.
 */

import React from 'react';


class BookAddComponent extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  onInputChange = (event) => {
    const target = event.target;
    const name = target.name;
    const value = target.value;

    this.setState({ [name]: value });
  }

  onSave = (event) => {
    event.preventDefault();
    this.props.addBook(
      this.state.title,
      this.state.description,
      this.state.author
    );
  };

  render() {
    return (
      <form>
        <input
          name='title'
          type='text'
          placeholder='Book title'
          value={ this.state.title }
          onChange={ this.onInputChange }/><br />
        <textarea
          name='description'
          placeholder='description'
          value={ this.state.description }
          onChange={ this.onInputChange } /><br />
        <input
          name='author'
          placeholder='Book Author'
          value={ this.state.author }
          onChange={ this.onInputChange } /><br />
        <button onClick={ this.onSave }> Save Book </button>
      </form>
    )
  };
};


export default BookAddComponent;
