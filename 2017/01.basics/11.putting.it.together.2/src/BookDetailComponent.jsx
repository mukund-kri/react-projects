/*
 * Show the details of a given book passed in as a parameter.
 */

import React from 'react';

class BookDetailComponent extends React.Component {

  render() {
    return (
      <div>
        <h3>Book { this.props.book.id } details</h3>
        <table>
          <tbody>
            <tr>
              <th>Title</th>
              <td>{ this.props.book.title }</td>
            </tr>
            <tr>
              <th>Description</th>
              <td>{ this.props.book.description }</td>
            </tr>
            <tr>
              <th>Title</th>
              <td>{ this.props.book.author }</td>
            </tr>
          </tbody>
        </table>
        <button onClick={ this.props.listView }>Back to List view</button>
      </div>
    )
  }
};

export default BookDetailComponent;
