import React             from 'react';

import BookCRUDComponent from './BookCRUDComponent';

// Mocked data. Some sample books; in json format.
import books from './books.json';


class Main extends React.Component {

  render() {
    return (
      <BookCRUDComponent books={ books }/>
    )
  }
}

export default Main;
