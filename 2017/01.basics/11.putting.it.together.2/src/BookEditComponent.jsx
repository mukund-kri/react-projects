/*
 * Show an edit form with a given books values populated.
 * call the parents update function to update the values of the given book.
 */

import React from 'react';


class BookEditComponent extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      book: props.book
    };
  };

  onInputChange = (event) => {
    const target = event.target;
    const name = target.name;
    const value = target.value;

    // Defintily not pretty!!!! TODO: think of a better way to do this!
    this.setState({
      book: {
        ...this.state.book,
        [name]: value
      }
    });
  };

  onSave = (event) => {
    event.preventDefault();
    console.log(this.state);
    this.props.updateBook(
      this.state.book.id,
      this.state.book
    )
  };

  render() {
    return (
      <form>
        <input
          name='title'
          type='text'
          placeholder='Book title'
          value={ this.state.book.title }
          onChange={ this.onInputChange }/><br />
        <textarea
          name='description'
          placeholder='description'
          value={ this.state.book.description }
          onChange={ this.onInputChange } /><br />
        <input
          name='author'
          placeholder='Book Author'
          value={ this.state.book.author }
          onChange={ this.onInputChange } /><br />
        <button onClick={ this.onSave }> Save Book </button>
      </form>
    )
  };
};

export default BookEditComponent;
