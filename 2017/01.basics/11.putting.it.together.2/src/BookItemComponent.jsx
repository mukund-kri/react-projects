/*
 * Takes care of rendering a single book of the Book List Component.
 */

import React from 'react';


class BookItemComponent extends React.Component {

  constructor(props) {
    super(props);
  };

  showBook = (e) =>  {
    this.props.showBook( this.props.book.id );
  };

  delBook = (e) => {
    this.props.delBook( this.props.book.id );
  };

  editBook = (e) => {
    this.props.editView(this.props.book.id);
  };

  render() {
    return (
      <div>
        { this.props.book.title }
        <button onClick={ this.showBook }>Show Book</button>
        <button onClick={ this.delBook }>Delete</button>
        <button onClick={ this.editBook }>Edit</button>
      </div>
    )
  };
};

export default BookItemComponent;
