/*
 * Take care of UI of listing the books an nothing else.
 */

import React             from 'react';

import BookItemComponent from './BookItemComponent';


class BookListComponent extends React.Component {

  render() {
    return (
      <div>
        { this.props.books.map((book) =>
          <BookItemComponent
            book={ book }
            key={ book.id }
            showBook={ this.props.showBook }
            delBook={ this.props.delBook }
            editView={ this.props.editView }
            />
        )}
        <button onClick={ this.props.addView }>Add a Book</button>
      </div>
    )
  };
};

export default BookListComponent;
