/*
 * Putting together all that I did in the previous 10 section.
 * A full fledged CRUD UI for books, where the user can list, add, delete and
 * update books.
 */

import React from 'react';

import BookListComponent   from './BookListComponent';
import BookAddComponent    from './BookAddComponent';
import BookDetailComponent from './BookDetailComponent';
import BookEditComponent   from './BookEditComponent';


class BookCRUDComponent extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      view: 'listing',   // Show only the list view on startup
      books: props.books
    }
  }

  showBook = (bookId) => {
    var selectedBook = this.state.books.filter((book) => book.id == bookId)[0];
    this.setState({selectedBook, view: 'detail'});
  }

  listView = () => {
    this.setState({ view: 'listing' })
  }

  addView = () => {
    this.setState({ view: 'add' })
  }

  editView = (bookId) => {
    var selectedBook = this.state.books.filter((book) => book.id == bookId)[0];
    this.setState({selectedBook, view: 'edit'});
  }

  delBook = (bookId) => {
    var otherBooks = this.state.books.filter((book) => book.id != bookId);
    this.setState({ books: otherBooks })
  }

  addBook = (title, description, author) => {

    const books = this.state.books;
    let lastBook = books.slice(-1)[0]
    var nxtBookId = 0
    if(lastBook) {
      nxtBookId = lastBook.id + 1;
    }

    const newBook = {
      id: nxtBookId,
      title,
      description,
      author
    }

    this.setState({
      view: 'list',
      books: this.state.books.concat(newBook)
    })
  }

  updateBook = (bookId, book) => {
    var selectedBook = this.state.books.filter((book) => book.id == bookId)[0];
    selectedBook.title = book.title;
    selectedBook.description = book.description;
    selectedBook.author = book.author;

    this.setState({ view: 'list' })
  }

  render() {
    switch (this.state.view) {
      case 'add':
        return <BookAddComponent addBook={ this.addBook } />
      case 'edit':
        return <BookEditComponent
          book={ this.state.selectedBook }
          updateBook={ this.updateBook }/>
      case 'detail':
        return <BookDetailComponent
          book={ this.state.selectedBook }
          listView={ this.listView } />
      case 'listing':
      default:
        return (
          <BookListComponent
            books={ this.state.books }
            showBook={ this.showBook }
            delBook={ this.delBook }
            addView={ this.addView }
            editView={ this.editView }
          />
        )

    }

  }

}


export default BookCRUDComponent;
