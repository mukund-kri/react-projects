# React Projects

A set of react projects that go from simple to complex. This projects are
primarily aimed at teaching React to total novices.

The following is a list of projects. The name of the projects mostly correspond
to the folder containing it.

## 01.basics

A React.js tutorial series built with the latest and greatest (as of Oct 2017)
tech ie. es2017, react 16.0, webpack 3.6 and so on. This progresivly explains
the core concept of React with out getting bogged down down with the deatils
of the full blown API of React.

It also has a few project ideas to work on to master the basics of react.

## 02.basics.ext

[TODO] A few essential basics over the basics.

 1. Testing. Show how to test your react components.
 2. Bootstrap CSS. Make your app more usable with Bootstrap.
 3. React+Redux. Centralize your state with the popular redux lib.
 4. Full Stack. React+Redux+Express+Mongo (MERN)

## 03.redux.basics

[TODO] Redux tutorial.
