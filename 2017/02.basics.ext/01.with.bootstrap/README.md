# Adding Bootstrap

In this project, I explore how use bootstrap for styling React applications.
This project in particular has the simplest possible react component, a
`hello world` component. Instead it focuses on how to build a `CSS` pipeline.
The way this project is setup is totally opinionated to how I want to do this.
There are infinite other ways to do it, but this way works for me.

## Parts

What and How I've wired up this project ...

* SASS, all my styling is done with sass.
* I've used Bootstrap from the npm package, as opposed to from a CDN. This is to
give me flexibility when I start customizing over the default Bootstrap.
* `ExtractTextPlugin` to collect all css into a single file.
* `css-loader` and `sass-loader`, to process sass files.

## Notable Changes

* `webpack.config.babel.js` :: Has a lot more code, for processing `.sass` files.
* `index.html` :: Is now a full-blown bootstrap web page, with top menu, side bar and such.
* `styles/` folder :: Where I store all my custom styles (in `sass` of-course).
