/*
 * This webpack config file adds SCSS pipeline over the basic config, that
 * I used with 01.basics series.
 *
 * Features:
 * 1. scss-loader :: compile scss to css
 * 2. css-loader  :: make CSS loadable as a JS module
 * 3. extract-text-webpack-plugin :: to collect all CSS into a seperate file
 */

import path              from 'path'
import ExtractTextPlugin from 'extract-text-webpack-plugin'


// This object tells webpack what to do and how to do it
const config = {

  // This file is where webpack starts it's work
  entry: path.resolve(__dirname, 'src/index.js'),

  // End result of webpack's work is dumped into this file
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'build.js'
  },

  module: {
    // Tells webpack which loader to use, based on the extension of the file.
    // In this case use babel for .js and .jsx files.
    // .scss files a put through the css pipeline
    rules: [
      { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
      { test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/ },
      {
        test: /\.sass$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'sass-loader']
        })
      }
    ]
  },

  // Use the ExtractTextPlugin to dump all CSS into a seperate css file.
  plugins: [
    new ExtractTextPlugin({
      filename: 'style.css'
    })
  ],

  resolve: {
    // Allows use to ommit the extension when importing a file.
    // we just import / require .. './filename' instead of './filename.js' or
    // './filename.jsx'
    extensions: ['.jsx', '.js']
  },

  // Turn debugging on, so we can debug in es6 and not the transpiled es5
  devtool: "source-map"
}

export default config;
