import React    from 'react'
import ReactDOM from 'react-dom'

import Hello    from './components/Hello'

// Just so that the css is built and a style.css is generated
import '../styles/main.sass'


// Attach React to the dom @ div with ID 'main'
ReactDOM.render(
  <Hello />,
  document.getElementById('main')
)
