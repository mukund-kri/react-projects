import gulp from 'gulp';
import log from 'fancy-log';
import colors from 'ansi-colors';
import webpack from 'webpack-stream';
import gls from 'gulp-live-server';

import webpackConfig from './webpack.config.babel.js';


const SERVER_ENTRY_POINT = 'server/index.js';

const server = gls(SERVER_ENTRY_POINT);


function runAndWatchServer() {
  gulp.watch(['server/**/*.js'], gulp.series(runAndWatchServer));
  server.start();
}

gulp.task('server', runAndWatchServer);
