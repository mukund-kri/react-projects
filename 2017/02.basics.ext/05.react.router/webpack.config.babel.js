/*
 * A simple webpack config file. Notice the extension of this file is
 * .babel.js instead of plain .js, the extra babel in the name let's us write
 * this config file in full es6.
 */
import path    from 'path';

// Webpack config object
const config = {

  entry: path.resolve(__dirname, 'src/index.js'),
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'build.js'
  },

  module: {
    loaders: [
      { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
      { test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/ }
    ]
  },

  resolve: {
    // Lets us omit extensions when importing
    extensions: ['.jsx', '.js']
  },

  // Turn on dev mode
  devtool: 'source-map'
};

export default config;
