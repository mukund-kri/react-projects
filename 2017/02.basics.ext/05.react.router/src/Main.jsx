import React, {Component} from 'react';
import NameComponent from './NameComponent';
import MessageComponent from './MessageComponent';
import NameStatefulComponent from './NameStatefulComponent';


class Main extends Component {

  render() {
    return (
      <div>
        <NameComponent />
        <MessageComponent msg={'Greetings'} />
        <NameStatefulComponent name={'Mukund'} />
      </div>
    );
  }
}


export default Main;
