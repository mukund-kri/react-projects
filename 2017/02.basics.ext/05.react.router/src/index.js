import React    from 'react';
import ReactDOM from 'react-dom';

import Main     from './Main';


ReactDOM.render(
  <Main name='John Doe' />,
  document.getElementById('main')
);
