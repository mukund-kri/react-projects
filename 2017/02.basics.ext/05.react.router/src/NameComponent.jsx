import React from 'react';
import PropTypes from 'prop-types'; 

const NameComponent = ({name}) => {
  return (
    <h3>Hello {name}</h3>
  );
};

NameComponent.propTypes = {
  name: PropTypes.string.isRequired
};

NameComponent.defaultProps = {
  name: 'John Doe'
};

export default NameComponent;

