import React, { Component } from 'react';
import PropTypes            from 'prop-types'; 

class TestSFComponent extends Component {

  constructor(props) {
    super(props);

    // set state here
    this.state = {name: props.name};
  }

  render() {
    return (
      <h1>Hello {this.state.name}</h1>
    );
  }
}

TestSFComponent.propTypes = {
  // Write your prop types here
  name: PropTypes.string.isRequired
};

TestSFComponent.defaultProps = {
  // Write the default values of your props here
};

export default TestSFComponent;
