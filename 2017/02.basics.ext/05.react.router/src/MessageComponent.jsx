import React     from 'react';
import PropTypes from 'prop-types'; 

const MessageComponent = ({msg}) => {
  return (
    <h1>Prop Got: {msg}</h1>
  );
};

MessageComponent.propTypes = {
  msg: PropTypes.string.isRequired
};

MessageComponent.defaultProps = {
  // Write the default values of your props here
};

export default MessageComponent;
