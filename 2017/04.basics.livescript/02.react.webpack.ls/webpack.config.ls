# Webpack config, written in LiveScript. Webpack will automaticall pick this up
# when .js version is not avaliable.

require! 'path'


module.exports =

  entry: './src/index.ls'

  output:
    filename: 'build.js'
    path: path.resolve __dirname, 'build'

  module:
    rules: [
      * test: /\.ls$/
        use: \livescript-loader
        exclude: /node-modules/
    ]

  resolve:
    extensions: <[.ls .js]>

  devtool: \source-map
