require! {
  \react : React,
  \react-dom : react-dom
}

$ = React.create-element

require! {
  \./Main : { Main }
}

main = $ Main, name: \John
el = document.get-element-by-id 'main'

react-dom.render main, el
