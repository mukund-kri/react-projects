# O6.Events

In this project we explore how to use React events and how to tie them with
DOM events.

This project contains the following examples

## Simple Event Handling

#### AlertComponent

A very basic example; an alert is popped up on click of a button.

#### ConsoleLogComponent

Another basic component; this time I use a stateful, class based component and
I log a message to the console on button click.

## Events and state

The following three examples show how we can accomplish useful features by
using events in conjunction with state.

#### EventsAndState

Change the state as per user button clicks.

#### ResetableCountdown

A simple countdown timer, with a button which resets the countdown to the
initial value on click of a button.

#### StopableClock

Same as the clock component given in the section `05.state`, with one addition
a button that 'Pauses' the clock.
