import { connect } from 'react-redux'

import CounterComponent from '../components/CounterComponent'


// Function that converts the redux state into props that can be injected into
// the CounterComponent as props.
// In this simple case it state and count are the same thing
function mapStateToProps(state) {
  return {
    count: state
  }
}

// map state dispatches onto component events
function mapDispatchToProps(dispatch) {
  return {
    onIncrementClick: () => {
      dispatch({ type: "INCREMENT" })
    },

    onDecrementClick: () => {
      dispatch({ type: "DECREMENT" })
    }
  }
}

// Bundle the mapStateToProps and mapDispatchToProps with the component to
// create a container.
const CounterContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(CounterComponent)

export default CounterContainer
