import React from 'react'


const CounterComponent = ({ count, onIncrementClick, onDecrementClick }) => {

  return (
    <div className="component">
      <h3>{ count }</h3>
      <button onClick={ onIncrementClick }> + </button>
      <button onClick={ onDecrementClick }> - </button>
    </div>
  )
}

export default CounterComponent
