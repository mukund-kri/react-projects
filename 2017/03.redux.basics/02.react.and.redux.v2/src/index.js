import React           from 'react'
import ReactDOM        from 'react-dom'
import { createStore } from 'redux'
import { Provider }    from 'react-redux'

import Counter         from './containers/Counter'
import counterReducer  from './reducers/CounterReducer'


// Create a redux store from the simple 'counterReducer'
const store = createStore(counterReducer)

ReactDOM.render(
  <Provider store={ store } >
     <Counter />
  </Provider>,
  document.getElementById('main')
)
