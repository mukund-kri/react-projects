/*
 * This example is directly lifted from the redux readme.
 */

import { createStore } from 'redux';

/*
 * CONCEPT 1: THE REDUCER.
 *
 * The job of the reducer is to compute the the new state given the old state
 * and an action. There is no side effects. So ...
 *
 * Current State + Action --> Return New State.
 */
const counter = (state = 0, action) => {
  switch(action.type) {
  case 'INCREMENT':
    return state + 1
  case 'DECREMENT':
    return state - 1
  default:
    return state
  }
}

/*
 * Use Redux api to create a store for the `GLOBAL SINGLE APP STATE`
 */
let store = createStore(counter);


/*
 * A single subscribe that gets fired every time an action is sent to the
 * store.
 */
store.subscribe(() =>
  console.log(store.getState())
)

/* The following examples are of 'dispatching' (sending) actions to the reducer
 */

// Send 'INCREMENT' to the reducer. In this case there is no state yet
// so a state of 0 is assumed (default parameter on reducer function)
store.dispatch({ type: 'INCREMENT' })

// Send 'INCREMENT' again
store.dispatch({ type: 'INCREMENT' })

// Send a 'DECREMENT' now. Should decrease the value of state by 1.
store.dispatch({ type: 'DECREMENT' })

// State should not be changed when we encounter an unknown action type
store.dispatch({ type: 'DUMMY' })
