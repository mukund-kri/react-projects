import React from 'react'

import store from '../state/store'


export default function Incrementer() {
  return (
    <button onClick={() => store.dispatch({type: 'INCREMENT'}) }>
      Increment
    </button>
  )
}
