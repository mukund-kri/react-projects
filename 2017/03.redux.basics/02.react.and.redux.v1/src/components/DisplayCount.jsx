import React from 'react'

import store from '../state/store'


export default function DisplayCount() {
  return (
    <div>
      <h3>Count :: { store.getState() }</h3>
    </div>
  )
}
