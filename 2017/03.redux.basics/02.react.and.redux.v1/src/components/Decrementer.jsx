import React from 'react'

import store from '../state/store'


export default function Decrementer() {
  return (
    <button onClick={ () => store.dispatch({type: 'DECREMENT'}) }>
      Decrement
    </button>
  )
}
