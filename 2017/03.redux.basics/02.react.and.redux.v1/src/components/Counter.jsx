/*
 * Just count the number of times a user clicks the button. Serves as a simple
 * demo of events + state.
 */
import React from 'react';

import DisplayCount from './DisplayCount'
import Incrementer  from './Incrementer'
import Decrementer  from './Decrementer'


export default function Counter() {
  return (
    <div className="component">
      <h1>React+Redux counter</h1>
      <DisplayCount />
      <Incrementer />
      <Decrementer />
    </div>
  )
};
