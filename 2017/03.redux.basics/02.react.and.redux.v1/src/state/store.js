import { createStore } from 'redux'
import { counter } from './reducers'


// A very simple store
const store = createStore(counter)

export default store
