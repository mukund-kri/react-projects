import ReactDOM from 'react-dom';

import Counter  from './components/Counter';
import store    from './state/store';


const render = () => ReactDOM.render(
  <Counter />,
  document.getElementById('main')
);

render();
store.subscribe(render);
