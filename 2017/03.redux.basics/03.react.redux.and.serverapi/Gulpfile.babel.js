import gulp from 'gulp';
import log from 'fancy-log';
import colors from 'ansi-colors';
import webpack from 'webpack-stream';
import gls from 'gulp-live-server';

import webpackConfig from './webpack.config.babel.js';


// Declare and start the express server with 'server.js' as entry point
const server = gls('server/server.js');

function startServer() {

  gulp.watch(['server/**/*.js'], gulp.series(startServer));
  server.start();
}

function runWebpack() {
  return gulp.src('frontend/index.js')
    .pipe(webpack(webpackConfig))
    .pipe(gulp.dest('./build'));
}

function runExpressInDevMode(done) {
  log(colors.green("Running development setup (webpack & express)"));

  startServer();

  // Restart the live server on change of any js code inside the server
  // folder
  gulp.watch(['server/**/*.js'], gulp.series(startServer));

  done();
};

gulp.task('server', runExpressInDevMode);

const develop = gulp.series(runWebpack, runExpressInDevMode);
gulp.task('default', develop);
