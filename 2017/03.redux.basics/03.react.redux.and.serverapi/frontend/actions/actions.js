
// User ACTIONS

// Select a subreddit to display
export const SELECT_SUBREDDIT = "SELECT_SUBREDDIT";

export function selectSubreddit(subreddit) {
  return {
    type: SELECT_SUBREDDIT,
    subreddit
  };
};

// Reload a subreddit
export const INVALIDATE_SUBREDDIT = "INVALIDATE_SUBREDDIT";

export function invalidateSubreddit(subreddit) {
  return {
    type: INVALIDATE_SUBREDDIT,
    subreddit
  };
};

// Request for posts event
export const REQUEST_POSTS = "REQUEST_POSTS";

export function requestPosts(subreddit) {
  return {
    type: REQUEST_POSTS,
    subreddit
  };
};

// On receive posts successfully from reddit
export const RECEIVE_POSTS = "RECEIVE_POSTS";

export function receivePosts(subreddit, json) {
  return {
    type: RECIEVE_POSTS,
    subreddit,
    posts: json.data.children.map(child => child.data),
    receivedAt: Date.now()
  };
};

