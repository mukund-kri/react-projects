/*
 * Web pack config for ""redux and server api"" example. Features ...
 * 1. es6 and JSX (via babel) loader
 * 2. style sheets with scss
 * 3. by default its in debug mode.
 */

import { resolve } from 'path';


const config = {

  entry: resolve(__dirname, 'frontend/index.js'),

  output: {
    path: resolve(__dirname, 'build'),
    filename: 'build.js'
  },

  module: {
    loaders: [
      { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
      { test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/ }
    ]
  },

  resolve: {
    extensions: ['.jsx', '.js']
  },

  devtool: "source-map",

  watch: true,
  watchOptions: {
    ignored: [/node_modules/, "server"]
  }

};

export default config;
